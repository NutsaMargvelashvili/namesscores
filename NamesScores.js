import { products } from './Names.js';

const namesScores = arr => {
    let n = 0;
    let result = 0;
    arr.sort(); 
    for(let i = 0; i < arr.length; i++){
        for(let j = 0; j < arr[i].length; j++){
          n += arr[i].charCodeAt(j) - 64;
        };
        result += n * (i + 1);
        n = 0;
    };                                                                                                                                                                                                                                                                                                                                                                                                        
    return result;    
};

products().then(result => {  
    console.log(namesScores(result));
})


